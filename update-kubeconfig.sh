#!/usr/bin/env bash

# Based on https://ahmet.im/blog/authenticating-to-gke-without-gcloud/

CLUSTER=$1
ZONE=$2

GET_CMD="gcloud container clusters describe $1 --zone=$2"

cat > .kubeconfig.yml <<EOF
apiVersion: v1
kind: Config
current-context: gke
contexts: [{name: gke, context: {cluster: cluster-1, user: user-1}}]
users: [{name: user-1, user: {auth-provider: {name: gcp}}}]
clusters:
- name: cluster-1
  cluster:
    server: "https://$(eval "$GET_CMD --format='value(endpoint)'")"
    certificate-authority-data: "$(eval "$GET_CMD --format='value(masterAuth.clusterCaCertificate)'")"
EOF
