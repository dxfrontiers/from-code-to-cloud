package de.digitalfrontiers.talk.exceptions;

public class IllegalPageNumberException extends RuntimeException {
    public IllegalPageNumberException(String message) {
        super(message);
    }
}
